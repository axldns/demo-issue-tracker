const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/', function (req, res) {
    db.query(null).then(r => res.send(r));
});

module.exports = router;
