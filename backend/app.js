const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const indexRouter = require('./routes/index');
const CREATE = require('./routes/create');
const READ = require('./routes/read');
const UPDATE = require('./routes/update');
const DELETE = require('./routes/delete');
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "..",'build')));

app.use('/', indexRouter);
app.use('/issues/create', CREATE);
app.use('/issues', READ);
app.use('/issues/update', UPDATE);
app.use('/issues/delete/:issueId', DELETE);

module.exports = app;
