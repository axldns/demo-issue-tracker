CREATE DATABASE `assignment`;
CREATE TABLE `assignment`.`issues` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `state` TINYINT NULL,
  `title` VARCHAR(45) NULL,
  `desc` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));
DROP USER IF EXISTS 'taskuser'@'localhost';
CREATE USER 'taskuser'@'localhost' IDENTIFIED WITH mysql_native_password BY 'schibsted';
GRANT SELECT,INSERT,UPDATE,DELETE ON assignment.issues TO 'taskuser'@'localhost';
FLUSH PRIVILEGES;