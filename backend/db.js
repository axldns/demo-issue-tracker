exports.query = (q) => {
    const mysql = require('mysql');
    const con = mysql.createConnection({
        host: 'localhost',
        user: 'taskuser',
        password: 'schibsted',
        database: 'assignment'
    });

    const defaultQuery = 'SELECT * FROM issues LIMIT 100';

    const execute = () => {
        return new Promise((resolve, reject) => {
            con.query(q, (error, results) => error ? reject(error) : resolve(results));
        })
    };
    const fetch = () => {
        return new Promise((resolve, reject) => {
            con.query(defaultQuery, (error, results) => error ? reject(error) : resolve(results));
        });
    };
    return q ? execute().then(fetch) : fetch();
};