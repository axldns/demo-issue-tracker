import * as React from "react";
import {BOXMODE, STATES} from "../Constants";

class LightBox extends React.Component {
    constructor(props) {
        super(props);
        this.onSave = this.onSave.bind(this);
    }

    onSave() {
        this.props.onBoxSave({
            id: this.props.currentIssue.id,
            state: this.props.currentIssue.state,
            title: document.querySelector("#creatorTitle").value,
            desc: document.querySelector("#creatorDesc").value
        });
    }

    get closeButton() {
        return <button className="close" onClick={this.props.onBoxClose}>Close</button>;
    }

    get saveButton() {
        return <button onClick={this.onSave}>Save</button>;
    }

    get deleteButton() {
        if (this.props.boxMode !== BOXMODE.EDIT) return null;
        return <button onClick={() => this.props.onBoxDelete(this.props.currentIssue)}>Delete</button>;
    }

    get progressButton() {
        const issue = this.props.currentIssue;
        if (this.props.boxMode !== BOXMODE.EDIT) return null;
        if (issue.state === STATES.DONE) return null;
        const label = `Progress to: ${["PENDING", "CLOSED"][issue.state]}`;
        return <button onClick={() => this.props.onBoxProgress(issue)}>{label}</button>
    }

    get titleInput() {
        return <input id="creatorTitle"
                      type="text"
                      className="creator input title"
                      placeholder="Issue title.."
                      defaultValue={this.props.currentIssue.title || ""}/>
    }

    get descInput() {
        return <textarea id="creatorDesc"
                         className="creator input desc"
                         placeholder="Issue desc.."
                         defaultValue={this.props.currentIssue.desc || ""}/>
    }

    get content() {
        return (
            <div className="lightbox">
                <div className="creator" disabled={this.props.boxMode === BOXMODE.LOCKED}>
                    {this.closeButton}{this.titleInput},{this.descInput}
                    <div className="buttons">
                        {this.deleteButton}{this.progressButton}{this.saveButton}
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return this.props.boxMode === BOXMODE.IDLE ? null : this.content;
    }
}

export default LightBox