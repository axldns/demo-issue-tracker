import * as React from "react";
import Column from "./Column";
import {STATES} from "../Constants";


class Board extends React.Component{
    render () {
        return (
            <div>
                <Column name="OPEN"
                        id={STATES.TODO}
                        issues={this.props.issues[STATES.TODO]}
                        onIssueCreate={this.props.onIssueCreate}
                        onIssueEdit={this.props.onIssueEdit}
                />
                <Column name="PENDING"
                        id={STATES.PROGRESS}
                        issues={this.props.issues[STATES.PROGRESS]}
                        onIssueCreate={this.props.onIssueCreate}
                        onIssueEdit={this.props.onIssueEdit}
                />
                <Column name="CLOSED"
                        id={STATES.DONE}
                        issues={this.props.issues[STATES.DONE]}
                        onIssueCreate={this.props.onIssueCreate}
                        onIssueEdit={this.props.onIssueEdit}
                />
            </div>
        )
    }
}
export default Board