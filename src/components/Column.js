import * as React from "react";
import Issue from "./Issue";

class Column extends React.Component {
    constructor(props) {
        super(props);
        this.onIssueCreate = this.onIssueCreate.bind(this);
        this.onIssueEdit = this.onIssueEdit.bind(this);
    }

    onIssueCreate() {
        this.props.onIssueCreate(this.props.id);
    }

    onIssueEdit(issue) {
        this.props.onIssueEdit(issue);
    }

    render() {
        return (
            <div className="column">
                <div className="column header">
                    <h2>{this.props.name}</h2>
                </div>
                <div className="column rail">
                    {this.props.issues.map(issue =>
                        <Issue key={issue.id} data={issue} onEdit={this.onIssueEdit}/>
                    )}
                </div>
                <div className="column footer" onClick={this.onIssueCreate}>+</div>
            </div>
        )
    }
}

export default Column