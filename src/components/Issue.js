import * as React from "react";

class Issue extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="issue" onClick={() => this.props.onEdit(this.props.data)}>
                <div className="title tile">
                    <b>{this.props.data.title}</b><br/>
                    {this.props.data.desc}
                </div>
            </div>
        )
    }
}

export default Issue