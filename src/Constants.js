exports.STATES = {
    TODO : 0,
    PROGRESS : 1,
    DONE : 2
};
exports.BOXMODE = {
    LOCKED: -1,
    IDLE : 0,
    CREATE : 1,
    EDIT : 2
};