import React from 'react';
import axios from 'axios';
import Board from "./components/Board";
import {BOXMODE, STATES} from "./Constants"
import LightBox from "./components/LightBox";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            boxMode: BOXMODE.IDLE,
            currentIssue: {},
            issues: this.emptyIssues
        };
        this.onIssueCreate = this.onIssueCreate.bind(this);
        this.onIssueEdit = this.onIssueEdit.bind(this);
        this.onBoxClose = this.onBoxClose.bind(this);
        this.onBoxSave = this.onBoxSave.bind(this);
        this.onBoxDelete = this.onBoxDelete.bind(this);
        this.onBoxProgress = this.onBoxProgress.bind(this);
        this.updateToIssueList = this.updateToIssueList.bind(this);
    }

    get emptyIssues() {
        return {
            [STATES.TODO]: [],
            [STATES.PROGRESS]: [],
            [STATES.DONE]: [],
        }
    }

    componentDidMount() {
        axios.get("/issues").then(this.updateToIssueList);
    }

    updateToIssueList({data}) {
        const issues = this.emptyIssues;
        data.forEach(issue => issues[issue.state].push(issue));
        this.setState({issues});
    }

    onIssueCreate(state) {
        this.setState({
            boxMode: BOXMODE.CREATE,
            currentIssue: {state}
        });
    }

    onIssueEdit(issue) {
        this.setState({
            currentIssue: issue,
            boxMode: BOXMODE.EDIT
        });
    }

    onBoxClose() {
        this.setState({
            boxMode: BOXMODE.IDLE,
            currentIssue: {}
        });
    }

    async onBoxProgress(issue) {
        this.setState({boxMode: BOXMODE.LOCKED});
        issue.state = Number(issue.state) + 1;
        await this.onBoxSave(issue);
    }

    async onBoxSave(issue) {
        this.setState({boxMode: BOXMODE.LOCKED});
        const url = this.state.boxMode === BOXMODE.CREATE ? "issues/create" : `issues/update`;
        const res = await axios.post(url, issue, {headers: {'Content-Type': 'application/json'}});
        this.onNetworkResponse(res);
    }

    async onBoxDelete({id}) {
        this.setState({boxMode: BOXMODE.LOCKED});
        const res = await axios.get(`/issues/delete/${id}`);
        this.onNetworkResponse(res);
    }

    onNetworkResponse(res) {
        console.log("GOT RESP", res);
        this.updateToIssueList(res);
        this.onBoxClose();
    }

    render() {
        return (
            <div className="App">
                <Board issues={this.state.issues}
                       onIssueCreate={this.onIssueCreate}
                       onIssueEdit={this.onIssueEdit}
                />
                <LightBox boxMode={this.state.boxMode}
                          currentIssue={this.state.currentIssue}
                          onBoxClose={this.onBoxClose}
                          onBoxSave={this.onBoxSave}
                          onBoxDelete={this.onBoxDelete}
                          onBoxProgress={this.onBoxProgress}
                />
            </div>
        );
    }
}

export default App;
